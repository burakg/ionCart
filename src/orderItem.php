<?php

namespace burakg\ionCart;

/**
 * Online shopping order elements class
 *
 * @author Burak M. Gurbuz
 */

use burakg\ion AS ion;
use burakg\ionLogger AS logger;
use burakg\ion\dbBase AS dbBase;

class orderItem extends dbBase {
	use dbBase\dbBaseTrait;

	public function init(){
		$this->properties->tableName = 'order_items';
		$this->properties->fields = array('order_id','product_id','product_name','product_price','product_currency', 'product_count', 'product_code', 'product_data', 'product_attributes');
	}

	public function bind_items($order_id){
		if($order_id > 0){
			foreach($_SESSION['my_cart']['products'] AS $product){
				$this->settings->presetValues = array(
					'order_id' => $order_id,
					'product_id' => $product['id'],
					'product_name' => $product['name'],
					'product_price' => $product['price'],
					'product_currency' => $product['currency'],
					'product_count' => $product['count'],
					'product_code' => $product['code'],
					'product_data' => $product['data'],
					'product_attributes' => $product['attributes'],
				);
				$this->save();
				$this->settings->id = 0;
			}
		}
	}
}