<?php

namespace burakg\ionCart;

use burakg\ion AS ion;
use burakg\ionLogger AS logger;
use burakg\ion\dbBase AS dbBase;

class shippingRates extends dbBase {
	use dbBase\dbBaseTrait;

	public function init() {
		$this->properties->tableName = 'shipping_rates';
		$this->properties->fields = ['name','region_id','carrier_name','range_min','range_max','price','currency','is_extra','discount'];

		$this->settings->regions = [
			'REGION_EUROPE' => [
				1 => ['Türkiye',1],
				2 => ['Germany',2],
				3 => ['Austria',2],
				4 => ['Belgium',2],
				5 => ['Denmark',2],
				6 => ['France',2],
				7 => ['Switzerland',2],
				8 => ['Sweden',2],
				9 => ['Spain',2],
				10 => ['Holland',2],
				11 => ['Netherlands',2],
				12 => ['Romania',2],
				13 => ['Estonia',2],
				14 => ['Kosovo',2],
				15 => ['Albania',2],
				16 => ['Croatia',2],
			],
			'REGION_AFRICA' => [
				21 => ['Morocco',3],
				22 => ['Gambia',3],
				23 => ['Ghana',3],
				24 => ['Libya',3],
				25 => ['Egypt',3],
			],
			'REGION_MID_EAST' => [
				31 => ['Qatar',3],
				32 => ['Saudi Arabia',3]
			]
		];
	}

	public function region_select(){
		$output = null;
		foreach($this->settings->regions AS $region => $countries){
			$output .= '<optgroup label="'.ion\helpers\phraser::get()->translate($region).'">';
			foreach($countries AS $code => $country)
				$output .= '<option value="'.$code.'">'.$country[0].'</option>';
			$output .= '</optgroup>';
		}

		return $output;
	}

	public function find_region($id,$returnRate=true,$desiWeight){
		foreach($this->settings->regions AS $region => $countries){
			if(array_key_exists($id,$countries)){
				if($returnRate === true){
					$list = $this->get_list(null,1,[
						"region_id" => $countries[$id][1],
						"is_extra" => "0",
						"range_min" => [$desiWeight,"<="],
						"range_max" => [$desiWeight,">="],
					]);
					if(!is_array($list)) {
						$list = $this->get_list(null, 1, [
							"region_id" => $countries[$id][1],
							"is_extra" => "0",
						], " ORDER BY range_min DESC");
						$list[0]['originalPrice'] = $list[0]['price']-($list[0]['price']*$list[0]['discount']);
						$list[0]['extra_needed'] = 1;

						$max = $list[0]['range_max'];
						$leftOver = $desiWeight-$max;

						$extraList = $this->get_list(null, 1, [
							"region_id" => $countries[$id][1],
							"is_extra" => 1,
						]);
						$count = $leftOver/$extraList[0]['range_max'];
						$leftOverPrice = (round($count)*$extraList[0]['price'])-($extraList[0]['price']*$extraList[0]['discount']);
						$list[0]['leftOverPrice'] = $leftOverPrice;
						$list[0]['price'] += $leftOverPrice;
						$list[0]['originalPrice'] += $leftOverPrice;
					}else{
						$list[0]['extra_needed'] = 0;
					}
					return (is_array($list)) ? $list[0] : false;
				}else{
					return $countries[$id];
				}
			}
		}

		return false;
	}
}