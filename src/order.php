<?php

namespace burakg\ionCart;

/**
 * Online shopping order class
 *
 * @author Burak M. Gurbuz
 */

use burakg\ion AS ion;
use burakg\ion\dbBase AS dbBase;
use ionApp\website\member;
use ionApp\website\product;
use ionApp\website\notification;
use Spipu\Html2Pdf\Html2Pdf;

class order extends dbBase {
    use dbBase\dbBaseTrait;
    public $colors = array(
        0 => 'default',
        1 => 'info',
        2 => 'success',
        3 => 'danger'
    ),
        $states = array(
        0 => 'Teklif hazırlanıyor',
        1 => 'Teklif verildi',
//		2 => 'ORDER_STATE_2',
//		3 => 'ORDER_STATE_3'
    ),
        $paymentStates = array(
        0 => 'PAYMENT_STATE_0',
        1 => 'PAYMENT_STATE_1'
    );

    public function init(){
        $this->properties->tableName = 'orders';
        $this->properties->fields = [
            'member_id',
            'email',
            'order_code',
            'cargo_code',
            'shipping_name',
            'shipping_address',
            'shipping_city',
            'shipping_county',
            'shipping_postcode',
            'billing_name',
            'billing_address',
            'billing_city',
            'billing_county',
            'billing_company',
            'billing_postcode',
            'phone_cell',
            'phone_other',
            'tcno',
            'is_gift',
            'shipping_data',
            'shipping_price',
            'tax_no',
            'tax_bureau',
            'grand_total',
            'tax_total',
            'currency',
            'payment_status',
            'status',
            'transaction_data',
            'order_notes',
            'orderdata',
            'token',
            'bound_user'
        ];
    }

    public function transaction_inject($result){
        $this->set_field('transaction_data',json_encode([
            'transaction_id' => $result->transaction->transaction_id,
            'external_id' => $result->transaction->external_id,
            'reference_id' => $result->transaction->reference_id,
            'payment_time' => $result->response->date_time,
            'last_digit' => $result->account->last_4_digits,
            'expires' => $result->account->expiry_year
        ]));
        $this->set_field('order_code',$result->transaction->external_id);
        $this->set_field('payment_status',1);
        $this->set_field('status',1);

        if($this->settings->id > 0){
            if(!is_array($this->settings->details) || count($this->settings->details) == 0)
                $details = $this->get();

            $ship = new shippingRates;
            $scountry = $ship->find_region($this->settings->details['shipping_country'],false,null);
            $bcountry = $ship->find_region($this->settings->details['billing_country'],false,null);
            $address = [
                'shipping' => $this->settings->details['shipping_first_name'].' '.$this->settings->details['shipping_last_name'].'<br>'
                    .$this->settings->details['shipping_address'].'<br>'
                    .$this->settings->details['shipping_county'].'<br>'.$this->settings->details['shipping_city'].'<br>'
                    .$scountry[0],
                'billing' => $this->settings->details['billing_first_name'].' '.$this->settings->details['billing_last_name'].'<br>'
                    .$this->settings->details['billing_address'].'<br>'
                    .$this->settings->details['billing_county'].'<br>'.$this->settings->details['billing_city'].'<br>'
                    .$bcountry[0]
            ];

            $n = new notification($details['language']);
            $n->order_new(
                $this->settings->details['billing_first_name'] . ' ' . $this->settings->details['billing_last_name'],
                $this->settings->details['email'],
                $result->transaction->external_id,
                $address
            );

        }
    }

    public function order_dispatch($ordercode,$cargocode){
        $this->set_field('cargo_code',$cargocode);
        $this->set_field('status',2);

        $shippingdata = json_decode($this->settings->details['shipping_data']);
        $n = new notification($this->settings->details['language']);

        $ship = new shippingRates;
        $scountry = $ship->find_region($this->settings->details['shipping_country'],false,null);
        $bcountry = $ship->find_region($this->settings->details['billing_country'],false,null);
        $address = [
            'shipping' => $this->settings->details['shipping_name'].'<br>'
                .$this->settings->details['shipping_address'].'<br>'
                .$this->settings->details['shipping_county'].'<br>'.$this->settings->details['shipping_city'].'<br>'
                .$scountry[0],
            'billing' => $this->settings->details['billing_name'].'<br>'
                .$this->settings->details['billing_address'].'<br>'
                .$this->settings->details['billing_county'].'<br>'.$this->settings->details['billing_city'].'<br>'
                .$bcountry[0]
        ];

        $n->order_dispatched(
            $this->settings->details['billing_name'],
            $this->settings->details['email'],
            $ordercode,
            $cargocode,
            $address,
            $shippingdata->carrier_name
        );
    }

    public function suspend_order(){
        $this->set_field('payment_status',0);
    }

    public function pay_order($transaction_id){
        $this->set_field('payment_status',1);
        $this->set_field('transaction_id',$transaction_id);
    }

    public function cancel_order(){
        $this->set_field('payment_status',2);
    }

    public function get_status_info($code){
        return array($code,$this->states[$code]);
    }

    protected function insert_callback(){
        $orderItem = new orderItem('front');
        $orderItem->bind_items($this->settings->id);

        $this->get();

        $member = new member;
        $member->get($this->settings->details['member_id']);

        $notification = new notification;
        $notification->order_new($member->details,$this->settings->details);
    }

    protected function update_callback(){

    }

    public function create_pdf(){
        $member = new member;
        $member->get($this->settings->details['member_id']);

        $user = new dbBase\user;
        $user->get($this->settings->details['bound_user']);
        $extra = json_decode($user->details['extradata']);

        $items = json_decode($this->settings->details['orderdata'],true);
        $prods = null;
        foreach($items AS $item){
            $prods .= '
<tr>
    <td>'.$item['name'].'</td>
    <td>'.$item['count'].'</td>
    <td align="right">'.number_format($item['price']*$item['tax_rate'],2).' TL</td>
    <td align="right">'.number_format($item['price'],2).' '.$item['currency'].'</td>
    <td align="right">'.number_format($item['price']*$item['count'],2).' '.$item['currency'].'</td>
</tr>';
        }
        $template = file_get_contents(ion\ion::get()->get_current_app('website')->CB_ROOT.'masterpages/proposal.php');
        $data = [
            '{code}' => $this->settings->details['order_code'],
            '{date}' => date('d.m.Y'),
            '{title}' => $this->settings->details['order_code'].' nolu Teklifiniz',
            '{body}' => '<table></table>',
            '{name}' => $member->details['name'],
            '{company}' => $this->settings->details['billing_name'],
            '{items}' => $prods,
            '{address}' => $this->settings->details['billing_address'],
            '{phone}' => $this->settings->details['phone_cell'],
            '{taxbureau}' => $this->settings->details['tax_bureau'],
            '{taxno}' => $this->settings->details['tax_no'],
            '{grandtotal}' => number_format((int)$this->settings->details['grand_total']-(int)$this->settings->details['tax_total'],2).' '.$this->settings->details['currency'],
            '{total}' => number_format($this->settings->details['grand_total'],2).' '.$this->settings->details['currency'],
            '{tax}' => number_format($this->settings->details['tax_total'],2).' '.$this->settings->details['currency'],
            '{bounduser}' => $user->details['firstname'].' '.$user->details['lastname'],
            '{boundphone}' => $extra->phone,
            '{root}' => CF_ROOT.ion\ion::get()->get_current_app('website')->CF_ROOT
        ];
        $template = str_replace(
            array_keys($data),
            array_values($data),
            $template
        );

        $app = ion\ion::get()->get_current_app('website');
        $upDir = $app->CB_ASSETS.'uploads/quotes/';

        $result = file_get_contents("http://api.html2pdfrocket.com/pdf?apikey=" . urlencode('15f3096e-d3e3-4b62-add8-3eed37ffe962') . "&OutputFormat=jpg&value=" . urlencode($template));
        file_put_contents($upDir.'Plotter-Teklif-'.$this->settings->details['order_code'].'.jpg', $result);

        return $template;
    }

    public function captcha_check(){
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LeQygsTAAAAAAYbvzgeigFrnU4JW7xXYCJZjLL_&response=".$_POST['g-recaptcha-response']);
        $response = json_decode($response, true);
        return ($response['success'] === true);
    }

    public function get_stats(){
        $output = [];

        $stmt = $this->doQuery()->prepare("SELECT * FROM {$this->properties->tableName}");
        $stmt->execute();

        $orderList = $stmt->fetchAll();

//		$output['totalIncome'] = $successful['totalIncome'];
        $output['totalOrders'] = count($orderList);
        $output['totalPending'] = count(array_filter($orderList,function($item){
            return ($item['status'] == 0);
        }));
        $output['totalDispatch'] = $output['totalOrders']-$output['totalPending'];

        return $output;
    }
}