<?php

namespace burakg\ionCart;

# Base extension: handles database operations and provides data to front
# @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
# Created on : 30.03.2014

use burakg\ion AS ion;
use burakg\ionLogger AS logger;
use burakg\ion\dbBase AS dbBase;

class payment extends dbBase {
	use dbBase\dbBaseTrait;

	public function init() {
		$this->properties->tableName = 'payment';
		$this->properties->fields = ['email','amount','currency','order_id','transaction_id','transaction_data'];
	}

}