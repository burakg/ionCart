<?php
/**
 * Created by PhpStorm.
 * User: burak
 * Date: 25.08.2015
 * Time: 00:21
 */

namespace burakg\ionCart;

use burakg\ion AS ion;

class iyzicoClient {
	private $productList = array();
	private $cartObject;

	private $tokenEndpoint = "https://iyziconnect.com/post/v1/";
	private $installmentEndpoint = "https://iyziconnect.com/installment/v1/";
	private $binCheckEndpoint = "https://api.iyzico.com/bin-check";
	private $statusEndpoint = "https://api.iyzico.com/getStatus";

	private $liveApiID = "";
	private $liveApiSecret = "";

	private $currency = 'TRY';

	private $response;

	/**
	 * @param $mode
	 * @param \burakg\ionCart\ionCart|string $cartObject
	 */
	public function __construct($mode,$cartObject){
		$this->mode = ($mode == 'live') ? 'live' : 'test';
		$this->cartObject = $cartObject;
	}

	public function test_card($bin){

	}

	/**
	 * @param $orderObject
	 */
	public function gettoken($orderObject) {
		$item_index = 1;
		foreach ($this->cartObject->get_products() as $product) {
			$this->productList['item_id_' . $item_index] = $product->id;
			$this->productList['item_name_' . $item_index] = $product->name;
			$this->productList['item_unit_quantity_' . $item_index] = $product->count;
			$item_index++;
		}
        $merchantApiId = $this->liveApiID;
        $merchantSecretKey = $this->liveApiSecret;
		$data['mode'] = $this->mode;
		$presAmount = $orderObject->details['grand_total'];
		$presUsage = "Order #" . $orderObject->details['order_code'];


		$installmentOptions = $this->config->get('iyzico_checkout_installment_options');
		$generateTokenArray = array(
			'api_id' => $merchantApiId,
			'secret' => $merchantSecretKey,
			'identification_transactionid' => uniqid("ion_cart_") . "_" . $this->session->data['order_id'],
			'payment_type' => 'CC.DB',
			'pres_amount' => $presAmount,
			'pres_currency' => $this->currency,
			'pres_usage' => $presUsage,
			'installment_option' => $installmentOptions
		);
		if (function_exists('curl_version')) {
			$paymentTokenResp = $this->_generateToken($generateTokenArray, $orderObject);
			$paymentTokenResp = json_decode($paymentTokenResp, true);

			if (!empty($paymentTokenResp['response']['state']) && $paymentTokenResp['response']['state'] == 'success') {
				$data['access_token'] = $paymentTokenResp['code_snippet'];
				$data['error'] = '';
			} else {
				$errorMessage = 'Token generation failed';
				if (!empty($paymentTokenResp['response']['error_message'])) {
					$errorMessage = $paymentTokenResp['response']['error_message'];
				}

				$data['error'] = $this->language->get($errorMessage);
				$data['access_token'] = '';
			}
		} else {
			$data['error'] = $this->language->get("Error_message_curl");
			$data['access_token'] = '';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	/**
	 *
	 * @param array $generateTokenArray
	 * @param \burakg\ionCart\order $orderObject
	 * @return array
	 */
	private function _generateToken($generateTokenArray, $orderObject) {
		$routeUrl = ion\ion::get()->front()->navigation()->node_url_by_attr('id',11);
		$postFieldsArray = array(
			'api_id' => ($this->mode == 'test') ? $this->testApiID : $this->liveApiID,
			'secret' => ($this->mode == 'test') ? $this->testApiSecret : $this->liveApiSecret,
			'mode' => $this->mode,
			'response_mode ' => 'SYNC',
			'external_id' => $orderObject->details['order_code'],
			'type' => $generateTokenArray['payment_type'],
			'amount' => $orderObject->details['grand_total'],
			'currency' => $this->currency,
			'return_url' => $routeUrl,
			'descriptor' => "Jetroll Siparişi - ".$orderObject->details['order_code']." - Taksim Ticaret",
			'customer_presentation_usage' => $generateTokenArray['pres_usage'],
			'customer_contact_email' => $orderObject->details['email'],
			'customer_first_name' => $orderObject->details['billing_first_name'],
			'customer_last_name' => $orderObject->details['billing_last_name'],
			'customer_company_name' => $orderObject->details['billing_company'],
			'customer_shipping_address_line_1' => $orderObject->details['shipping_address'],
			'customer_shipping_address_line_2' => null,
			'customer_shipping_address_zip' => $orderObject->details['shipping_postcode'],
			'customer_shipping_address_city' => $orderObject->details['shipping_city'],
			'customer_shipping_address_state' => $orderObject->details['shipping_county'],
			'customer_shipping_address_country' => 'Türkiye',
			'customer_billing_address_line_1' => $orderObject->details['billing_address'],
			'customer_billing_address_line_2' => null,
			'customer_billing_address_zip' => $orderObject->details['billing_postcode'],
			'customer_billing_address_city' => $orderObject->details['billing_city'],
			'customer_billing_address_state' => $orderObject->details['billing_county'],
			'customer_billing_address_country' => "Türkiye",
			'customer_contact_phone' => $orderObject->details['phone_other'],
			'customer_contact_mobile' => $orderObject->details['phone_cell'],
			'customer_contact_ip' => $_SERVER['REMOTE_ADDR'],
			'customer_language' => ion\ion::get()->curLang,
			'installment_count' => $generateTokenArray['installment_count']
		);
		$postFieldsArray = array_merge($postFieldsArray, $this->productList);
		$response = $this->curlCall($this->tokenEndpoint, "POST", $postFieldsArray);
		return $response;
	}

	/**
	 *
	 * @param string $url
	 * @param array $data
	 * @param string $method
	 * @return string
	 */
	public function curlCall($url, $data = array(), $method="post") {
		if ($url == null) {
			return "error. url not given.";
		}
		$fields_string = http_build_query($data);
		$ch = curl_init();

		switch ($method) {
			case "POST":
				curl_setopt($ch, CURLOPT_POST, count($url));
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
				break;
			case "GET":
				$url .= "?" . $fields_string;
				break;
			default:
				return "Invalid request method";
		}
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		$jsonResponse = curl_exec($ch);
		$err = curl_errno($ch);
		$errmsg = curl_error($ch);
		$header = curl_getinfo($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		$header ['errno'] = $err;
		$header ['errmsg'] = $errmsg;
		return $jsonResponse;
	}

	public function get_form($orderData){
		$ion = ion\ion::get();
		$url = "https://api.iyzico.com/v2/create";
		if($this->mode == 'test'){
			$app_id = $this->testApiID;
			$app_secret = $this->testApiSecret;
		}else{
			$app_id = $this->liveApiID;
			$app_secret = $this->liveApiSecret;
		}
		$data = "api_id=" . $app_id .
			"&secret=". $app_secret .
			"&customer_language=".$ion->curLang .
			"&mode=".$this->mode .
			"&external_id=" . time() .
			"&type=CC.DB" .
			"&installment=true" .
			"&amount=".str_replace('.','',$orderData['grand_total']).
			"&return_url=".$ion->front()->navigation()->node_url_by_attr('id',16).'?status=respond' .
			"&currency=TRY";
		$params = array('http' => array(
			'method' => 'POST',
			'header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $data
		));
		$ctx = stream_context_create($params);
		$fp = fopen($url, 'rb', false, $ctx);
		$response = @stream_get_contents($fp);
		$response = json_decode($response);

		return $response;
	}
}