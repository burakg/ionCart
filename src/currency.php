<?php

namespace burakg\ionCart;

/**
 * Retrieves daily currencies from TCMB daily currency database once per 3 hours.
 *
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 */

use burakg\ion AS ion;

class currency {
	private $url = 'http://www.tcmb.gov.tr/kurlar/today.xml', $cache_file, $cache_time;

	public function __construct(){
		$dataPath = ion\ion::get()->get_current_app()->DATA_ROOT;
		$this->cache_file = $dataPath.'currency.json';
		$this->cache_time = 60*60*3;
	}

	public function calc_currency($price, $sourceCurrency, $targetCurrency='TL'){
	    if($sourceCurrency == $targetCurrency)
	        return $price;

		if(time() - filemtime($this->cache_file) >= $this->cache_time){
			$currencies = $this->get_currencies_xml();
		}else{
			$currencies = $this->get_cache();
		}

		return ($targetCurrency == 'TL' || $targetCurrency == 'TRY') ? $price*$currencies[$sourceCurrency] : $price*$currencies[$sourceCurrency]/$currencies[$targetCurrency];
	}

	private function get_cache(){
		$json_data = file_get_contents($this->cache_file);
		$arr_data = json_decode($json_data);

		return (array)$arr_data;
	}

	private function get_currencies_xml(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $xml = curl_exec($ch);
        curl_close($ch);

		$currency = simplexml_load_string($xml);

		$usd = $currency->xpath('Currency[@Kod="USD"]');
		$eur = $currency->xpath('Currency[@Kod="EUR"]');

		$new_currencies = array(
			'USD' => (string)$usd[0]->BanknoteBuying[0],
			'EUR' => (string)$eur[0]->BanknoteBuying[0],
			'TRY' => 1,
			'TL' => 1
		);

		$json_data = json_encode($new_currencies);
		file_put_contents($this->cache_file, $json_data);

		return $new_currencies;
	}
}