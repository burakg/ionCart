<?php

namespace burakg\ionCart;

/**
 * User: Burak Mehmet Gurbuz
 * Date: 05.01.2014
 * Time: 12:09
 */

use burakg\ion AS ion;
use ionApp\website\member;
use ionApp\website\product;

class ionCart {
	public $formResponse;

	public function __construct(){
		ion\front\auth::get();

		$this->set_session();

		if(ion\helpers::get()->validate($_POST['productID'])->is_numeric() > 0){
			$data = new product;
			$data->get($_POST['productID']);
			$extraData = json_decode($data->details['extradata'],true);

			$cartProduct = new cartProduct(
			    $data->id,
                $data->details['title'],
                $data->details['price'],
                $data->details['currency'],
                $data->id.'-'.ion\helpers::get()->text($data->details['title'])->slug(),
                $extraData,
                [
                    'type' => $data->details['type'],
                    'imgext' => $data->details['imgext'],
                    'parentid' => $data->details['parentid']
                ],
                $data->details['tax_rate']
            );

			$this->add_product($cartProduct->product);
		}

		if(ion\helpers::get()->validate($_POST['removeID'])->is_numeric() > 0){
			$this->remove_product($_POST['removeID']);
		}

		if(ion\helpers::get()->validate($_POST['updateID'])->is_numeric() > 0 && ion\helpers::get()->validate($_POST['count'])->is_numeric() >= 0){
			$this->update_product($_POST['updateID'],$_POST['count']);
		}
	}

	private function set_session(){
		if(!isset($_SESSION['my_cart'])){
			$_SESSION['my_cart'] = array(
				'products' => array(),
				'total' => 0,
				'tax' => 0,
				'shipping' => 0,
				'grand_total' => 0
			);
		}
	}

	public function update_cart(){
		foreach($_SESSION['my_cart']['products'] AS $key => $product){
			if(ion\helpers::get()->validate($_POST['count_'.$key])->is_numeric() > 0){
				$_SESSION['my_cart']['products'][$key]['count'] = $_POST['count_'.$key];
			}else{
				$this->remove_product($key);
			}
		}

		if(isset($_POST['remove'])){
			foreach($_POST['remove'] AS $remove){
				$this->remove_product($remove);
			}
		}

		$this->grand_total();
	}

	public function add_product($data){
	    $check = $this->search_product($data['id']);
	    if(count($check) > 0){
	        $key = array_keys($check)[0];
	        $this->increase_product($key);
        }else{
		    $_SESSION['my_cart']['products'][time()] = $data;
        }
		$this->grand_total();

		return $this;
	}

	protected function search_product($id){
	    $filter = array_filter($_SESSION['my_cart']['products'],function($item) use($id){
            return $item['id'] == $id;
        });
	    return $filter;
    }

	public function increase_product($item_id){
		$_SESSION['my_cart']['products'][$item_id]['count'] = $_SESSION['my_cart']['products'][$item_id]['count']+1;
	}

	public function update_product($item_id,$amount){
	    if(array_key_exists($item_id,$_SESSION['my_cart']['products'])){
            if($amount > 0){
                $_SESSION['my_cart']['products'][$item_id]['count'] = $amount;

                $this->grand_total();
                $this->tax();
            }else{
                $this->remove_product($item_id);
                $this->grand_total();
            }
        }
        return $this;
	}

	public function remove_product($item_id){
		unset($_SESSION['my_cart']['products'][$item_id]);

		$this->grand_total();
	}

	public function empty_cart(){
		$_SESSION['my_cart']['products'] = [];

		$this->grand_total();
	}

	public function get_products(){
		return $_SESSION['my_cart']['products'];
	}

	public function grand_total(){
		$grandTotal = array();
		foreach($_SESSION['my_cart']['products'] AS $product){
			$grandTotal[] = $this->currency($product['price'],$product['currency'])*$product['count'];
		}

		$_SESSION['my_cart']['total'] = array_sum($grandTotal);
		$_SESSION['my_cart']['tax'] = $this->tax();
		$_SESSION['my_cart']['grand_total'] = array_sum($grandTotal)+$_SESSION['my_cart']['tax']+$this->shipping()['price'];
		return $_SESSION['my_cart']['grand_total'];
	}

	public function shipping($destination=null){
		if(ion\helpers::get()->validate($destination)->is_numeric() > 0){
			$rate = new shippingRates;

			$desi = $this->get_desi();

			$regionRates = $rate->find_region($destination,true,max($desi));

			$regionRates['price'] = round($this->currency($regionRates['price'],$regionRates['currency']),2);
			$_SESSION['my_cart']['shipping'] = $regionRates;
		}

		return $_SESSION['my_cart']['shipping'];
	}

	public function get_desi(){
		$desi = 0;
		$weight = 0;

		foreach ($this->get_products() as $product) {
			$extra = json_decode($product['data'],true);
			$desi += ($extra['length']*$extra['width']*$extra['height'])/3000*$product['count'];
			$weight += $extra['weight'];
		}

		return [(string)$desi,(string)$weight];
	}

	public function tax(){
		$items = $this->get_products();
		$currency = new currency;

		$taxTotal = 0;
		foreach($items AS $item){
		    $taxTotal += $currency->calc_currency($item['price'],$item['currency'])*$item['count']*$item['tax_rate'];
        }
        $_SESSION['my_cart']['tax'] = $taxTotal;

		return (string)$_SESSION['my_cart']['tax'];
	}

	public function currency($price,$currency){
		$data = new currency;
		$c_data = $data->calc_currency($price,$currency);

		return $c_data;
	}
}