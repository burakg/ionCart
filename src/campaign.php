<?php

namespace burakg\ionCart;

/**
 * Online shopping order class
 *
 * @author Burak M. Gurbuz
 */

use burakg\ion AS ion;
use burakg\ionAdmin\mediumManager;
use burakg\ionLogger AS logger;
use burakg\ion\dbBase AS dbBase;
use project\website\notification;

class campaign extends dbBase {
	use dbBase\dbBaseTrait;
	use dbBase\mediumTrait;

	public function init(){
		$this->properties->tableName = 'campaigns';
		$this->properties->fields = [
			'title',
			'date_start',
			'date_end',
			'campaign_code',
			'type',
			'related_products',
			'related_members',
			'related_categories',
			'price_min',
			'discount_type',
			'discount_amount',
			'imgext',
			'status',
		];
	}

	protected function create_callback(){
        $this->add_medium();
    }

    protected function update_callback(){
        $this->add_medium();
    }

    protected function delete_callback(){
        $data = new mediumManager;
        $data->contentId = $this->settings->id;
        $data->contentTable = $this->properties->tableName;
        $data->delete_by_content_id($this->settings->id);
    }
}