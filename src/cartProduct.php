<?php

namespace burakg\ionCart;

/**
 * User: Burak Mehmet Gurbuz
 * Date: 05.01.2014
 * Time: 15:45
 */

class cartProduct {
	public $product=array();

	public function __construct($id,$name=null,$price=0,$currency='TRY',$code=null,$data,$attributes,$taxRate=0.18){
		$this->product['id'] = $id;
		$this->product['name'] = $name;
		$this->product['price'] = $price;
		$this->product['currency'] = $currency;
		$this->product['tax_rate'] = $taxRate;
		$this->product['count'] = 1;
		$this->product['code'] = $code;
		$this->product['data'] = $data;
		$this->product['attributes'] = $attributes;
	}
}